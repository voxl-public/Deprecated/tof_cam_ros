#!/bin/bash

# Patch the camera configuration file on target to fix CSI lane issue
adb push camera_config.patch /home/root
adb shell "cd /system/etc/camera/ && patch camera_config.xml < \
  /home/root/camera_config.patch"

# Patch the downward camera launch file to specify the correct camera
adb push downward.patch /home/root
adb shell "cd /home/root/ros_ws/install/share/snap_cam_ros/launch/ && \
  patch downward.launch < /home/root/downward.patch"
